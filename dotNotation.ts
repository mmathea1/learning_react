const lordify = regularPerson => {
    console.log(`${regularPerson.firstname} of Canterbury`);
};

const regularPerson = {
    firstname: "Bill",
    lastname: "Wilson"
};

lordify(regularPerson);
