const lordify = ({firstname}) => {
    console.log(`${firstname} of Canterbury`);
};

const regularPerson = {
    firstname: "Bill",
    lastname: "Wilson"
};

lordify(regularPerson);
